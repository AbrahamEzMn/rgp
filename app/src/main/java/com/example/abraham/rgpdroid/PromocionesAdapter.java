package com.example.abraham.rgpdroid;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Abraham on 24/11/2016.
 */

public class PromocionesAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<Promociones> items;

    public PromocionesAdapter(Activity activity, ArrayList<Promociones> items) {
        this.activity = activity;
        this.items = items;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}

class Promociones
{
    public boolean isSelect;
    public String Descripcion;
}
