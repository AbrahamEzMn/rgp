package com.example.abraham.rgpdroid;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class FragmentReservacion extends Fragment {
    String promocionTexto;
    String textInit;
    TextView calendarTextView;
    TextView tiempoTextView;
    TextView promocionSelecionadaTextView;
    Button promocionesButton;

    public FragmentReservacion()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_reservacion, container, false);

        promocionSelecionadaTextView = (TextView) rootView.findViewById(R.id.promocionesSeleccionadasReservacionTextView);

        calendarTextView = (TextView) rootView.findViewById(R.id.calendarioReservacionesTextView);
        calendarTextView.setOnClickListener(new ViewGroup.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //To show current date in the datepicker
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker;
                mDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        // TODO Auto-generated method stub
                    /*      Your code   to get date and time    */
                        selectedmonth = selectedmonth + 1;
                        calendarTextView.setText("" + selectedday + "/" + selectedmonth + "/" + selectedyear);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Selecciona la Fecha");
                mDatePicker.show();
            }
        });

        tiempoTextView = (TextView) rootView.findViewById(R.id.tiempoRecervacionTextView);
        tiempoTextView.setOnClickListener(new ViewGroup.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        tiempoTextView.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, false);
                mTimePicker.setTitle("Selecciona la Hora");
                mTimePicker.show();
            }
        });

        promocionesButton = (Button)rootView.findViewById(R.id.promocionesReceservacionesButton);
        promocionesButton.setOnClickListener(new ViewGroup.OnClickListener() {

            @Override
            public void onClick(View v) {
                final CharSequence[] items = {"Cervesas 2x1","Cervesas 3x2","Tres Shot Tequila gratis"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        promocionSelecionadaTextView.setText(promocionTexto);
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });

                builder.setTitle("Selecciona la promocion");
                builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        promocionTexto = items[item].toString();
                    }
                });

                builder.show();
            }
        });

        return rootView;
    }
}
