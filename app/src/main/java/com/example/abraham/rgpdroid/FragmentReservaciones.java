package com.example.abraham.rgpdroid;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class FragmentReservaciones extends Fragment {

    ArrayList<Reservacion> list = new ArrayList<Reservacion>();

    public FragmentReservaciones()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_reservaciones, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.listReservacionesListView);
        list.add(new Reservacion("Bakanora","Abraham Esparza Moreno","4491291613",6,"25-11-2016","11:40",null));
        ReservacionAdapter adapter = new ReservacionAdapter(getActivity(),list);
        listView.setAdapter(adapter);

        return rootView;
    }
}
