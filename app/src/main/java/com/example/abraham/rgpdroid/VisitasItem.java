package com.example.abraham.rgpdroid;

/**
 * Created by Abraham on 23/11/2016.
 */

public class VisitasItem {

    public String NombreDelBar;
    public String Fecha;
    public String Hora;

    public VisitasItem(String NombreDelBar, String Fecha, String Hora)
    {
        this.NombreDelBar = NombreDelBar;
        this.Fecha = Fecha;
        this.Hora = Hora;
    }
}
