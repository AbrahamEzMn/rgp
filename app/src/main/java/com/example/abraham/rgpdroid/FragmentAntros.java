package com.example.abraham.rgpdroid;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


public class FragmentAntros extends Fragment {

    ArrayList<Category> category = new ArrayList<Category>();

    public FragmentAntros() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_bar_main, container, false);
        ListView lv = (ListView) rootView.findViewById(R.id.listView);

        String linea1 = "Información importante que quiere dar a conocer el bar número uno a las personas que se puede asistir a su negocio.";
        String linea2 = "Información importante que quiere dar a conocer el bar número dos a las personas que se puede asistir a su negocio.";

        category.add(new Category("1","Nombre Bar Uno","Slogan Bar Uno", ContextCompat.getDrawable(getActivity(),R.mipmap.bar_background), linea1));
        category.add(new Category("2","Nombre Bar Dos","Slogan Bar Dos", ContextCompat.getDrawable(getActivity(),R.mipmap.ic_launcher),linea2));

        CategoryAdapter adapter = new CategoryAdapter(getActivity(), category);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int pos = position;

            }
        });
        return rootView;
    }

}
