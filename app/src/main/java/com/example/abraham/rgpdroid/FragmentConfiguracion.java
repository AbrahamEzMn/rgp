package com.example.abraham.rgpdroid;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Abraham on 25/11/2016.
 */

public class FragmentConfiguracion extends Fragment{

    public FragmentConfiguracion()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_configuracion, container, false);

        return rootView;
    }

}
