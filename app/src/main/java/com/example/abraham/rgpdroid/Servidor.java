package com.example.abraham.rgpdroid;

import android.app.Activity;
import android.os.StrictMode;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by Abraham on 26/11/2016.
 */

public class Servidor {
    public static String IP  = "192.168.1.67:8080";
    private String CARPETA =  "RPG";

    Activity activity;

    public Servidor(Activity activity)
    {
        this.activity = activity;
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public boolean login(String usuario, String contra, boolean isFacebook)
    {
        try {
            String link = "http://" + IP + "/" + CARPETA + "/Login.php";
            String data = URLEncoder.encode("Usuario", "UTF-8") + "=" + URLEncoder.encode(usuario, "UTF-8");
            data += "&" + URLEncoder.encode("Contra", "UTF-8") + "=" + URLEncoder.encode(contra, "UTF-8");
            if(isFacebook)
                data += "&" + URLEncoder.encode("IsFacebook", "UTF-8") + "=" + URLEncoder.encode("si", "UTF-8");
            else
                data += "&" + URLEncoder.encode("IsFacebook", "UTF-8") + "=" + URLEncoder.encode("no", "UTF-8");

            URL url = new URL(link);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String result = reader.readLine();
            Toast.makeText(activity,result, Toast.LENGTH_LONG).show();

            JSONObject listaJSON = new JSONObject(result);
            String query_result = listaJSON.getString("code");

            if (query_result.equals("200"))
            {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception e)
        {
            Toast.makeText(activity, e.toString(), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean registro(String usuario, String contra, String email, String telefono, String fbId, String foto)
    {
        try {
            String link = "http://" + IP + "/" + CARPETA + "/Registro.php";
            String data = URLEncoder.encode("Usuario", "UTF-8") + "=" + URLEncoder.encode(usuario, "UTF-8");
            data += "&" + URLEncoder.encode("Email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8");
            data += "&" + URLEncoder.encode("Telefono", "UTF-8") + "=" + URLEncoder.encode(telefono, "UTF-8");
            data += "&" + URLEncoder.encode("Contra", "UTF-8") + "=" + URLEncoder.encode(contra, "UTF-8");
            data += "&" + URLEncoder.encode("FbId", "UTF-8") + "=" + URLEncoder.encode(fbId, "UTF-8");
            data += "&" + URLEncoder.encode("Foto", "UTF-8") + "=" + URLEncoder.encode(foto, "UTF-8");

            URL url = new URL(link);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String result = reader.readLine();
            Toast.makeText(activity,result, Toast.LENGTH_LONG).show();

            JSONObject listaJSON = new JSONObject(result);
            String query_result = listaJSON.getString("code");

            if (query_result.equals("200"))
            {
                return true;
            }
            else {
                return false;
            }
        }
        catch (Exception e)
        {
            Toast.makeText(activity, e.toString(), Toast.LENGTH_LONG).show();
            return false;
        }
    }
}

