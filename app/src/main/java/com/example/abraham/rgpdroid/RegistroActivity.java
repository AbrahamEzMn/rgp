package com.example.abraham.rgpdroid;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;


public class RegistroActivity extends Activity
{
    EditText nombreEditText ;
    EditText emailEditText;
    EditText telefonoEditText ;
    EditText contra;
    EditText verificarContraEditText;

    CallbackManager callbackManager;

    String fbId;

    ImageView fotoURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        TelephonyManager tMgr = (TelephonyManager)this.getSystemService(this.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        Toast.makeText(this, "Telefono " + mPhoneNumber, Toast.LENGTH_SHORT).show();

        nombreEditText = (EditText) findViewById(R.id.nombreRegistroTextEdit);
        emailEditText = (EditText) findViewById(R.id.emailRegistroTextEdit);
        telefonoEditText = (EditText) findViewById(R.id.telefonoRegistroTextEdit);
        contra = (EditText) findViewById(R.id.contraRegistroTextEdit);
        verificarContraEditText = (EditText) findViewById(R.id.verificarContraRegistroTextEdit);
        contra.setText("a");


        Button registroButton = (Button) findViewById(R.id.registrarRegistroButton);
        registroButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Servidor servidor = new Servidor(RegistroActivity.this);
                boolean is200 = servidor.registro(
                        nombreEditText.getText().toString(),
                        contra.getText().toString(),
                        emailEditText.getText().toString(),
                        telefonoEditText.getText().toString(),
                        "-1",
                        "0");
                if(is200)
                {
                    Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
                    overridePendingTransition(R.anim.activity_pull_in, R.anim.activity_pull_out);
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_pull_in, R.anim.activity_pull_out);
                }

            }
        });


        fotoURL = (ImageView) findViewById(R.id.imageView6);

        LinearLayout facebookBtn = (LinearLayout) this.findViewById(R.id.registrarFacebookRegistroButton);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", object.toString());
                                        try {

                                            fbId = object.getString("id");
                                            nombreEditText.setText(object.getString("name"));

                                            emailEditText.setText(object.getString("email"));
                                            Servidor servidor = new Servidor(RegistroActivity.this);
                                            boolean is200 = servidor.registro(
                                                    object.getString("name"),
                                                    "",
                                                    object.getString("email"),
                                                    "0",
                                                    object.getString("id"),
                                                    "0");
                                            if(is200)
                                            {
                                                Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
                                                overridePendingTransition(R.anim.activity_pull_in, R.anim.activity_pull_out);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.activity_pull_in, R.anim.activity_pull_out);
                                            }

                                            /*
                                            JSONObject objPicture = new JSONObject(object.getString("picture"));
                                            JSONObject objData = new JSONObject(objPicture.getString("data"));
                                            String urlFoto =  objData.getString("url");
                                            fotoURL.setImageBitmap(LoadImageFromWebOperations(urlFoto));
                                            Log.v("LoginActivity", objData.toString());

                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            LoadImageFromWebOperations(urlFoto).compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream .toByteArray();
                                            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                                            Log.v("LoginActivity", encoded);
                                            */

                                            LoginManager.getInstance().logOut();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday,picture");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(RegistroActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(RegistroActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        facebookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logInWithReadPermissions(RegistroActivity.this, Arrays.asList("public_profile", "email", "user_birthday"));
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_pull_in, R.anim.activity_pull_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public static Bitmap LoadImageFromWebOperations(String urlString) {
        try {
            URL url = new URL(urlString);
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            return bmp;
        } catch (Exception e) {
            return null;
        }
    }
}
