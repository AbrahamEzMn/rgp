package com.example.abraham.rgpdroid;

import android.graphics.drawable.Drawable;

/**
 * Created by Abraham on 13/11/2016.
 */

public class Category {
    private String title;
    private String categoryId;
    private String description;
    private Drawable imagen;

    private String info;
    private boolean selectInfo;

    public Category() {
        super();
    }

    public Category(String categoryId, String title, String description, Drawable imagen, String info) {
        super();
        this.title = title;
        this.description = description;
        this.imagen = imagen;
        this.categoryId = categoryId;

        this.info = info;
        this.selectInfo = false;
    }


    public String getTitle() {
        return title;
    }
    public void setTittle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Drawable getImage() {
        return imagen;
    }
    public void setImagen(Drawable imagen) {
        this.imagen = imagen;
    }

    public String getCategoryId(){return categoryId;}
    public void setCategoryId(String categoryId){this.categoryId = categoryId;}

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    public boolean getSelectInfo() {
        return selectInfo;
    }
    public void setSelectInfo(boolean selectInfo) {
        this.selectInfo = selectInfo;
    }
}
