package com.example.abraham.rgpdroid;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class CategoryAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<Category> items;

    public CategoryAdapter(Activity activity, ArrayList<Category> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
    }

    public void addAll(ArrayList<Category> category) {
        for (int i = 0; i < category.size(); i++) {
            items.add(category.get(i));
        }
    }

    @Override
    public Object getItem(int arg0) {
        return items.get(arg0);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.item_bar, null);
        }

        Category dir = items.get(position);

        final LinearLayout contentLayout = (LinearLayout) v.findViewById(R.id.contentItemBar);

        TextView title = (TextView) v.findViewById(R.id.category);
        title.setText(dir.getTitle());

        TextView description = (TextView) v.findViewById(R.id.texto);
        description.setText(dir.getDescription());

        ImageView imagen = (ImageView) v.findViewById(R.id.imageView13);
        imagen.setImageDrawable(dir.getImage());

        TextView information = (TextView) v.findViewById(R.id.infoItemBarTextView);
        information.setText(dir.getInfo());

        imagen.setClickable(true);
        imagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Se preciono el logo");
            }
        });

        LinearLayout informacionIco = (LinearLayout) v.findViewById(R.id.MainInformacionLinearLayout);
        informacionIco.setOnClickListener(new ViewGroup.OnClickListener() {
            boolean i = false;

            @Override
            public void onClick(View v) {
                i = !i;
                if (i) {
                    contentLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    v.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
                } else {
                    contentLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,0));
                    v.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        });
        LinearLayout direccion = (LinearLayout) v.findViewById(R.id.MainDireccionLinearLayout);
        direccion.setOnClickListener(new ViewGroup.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:21.876920,-102.261040?q=21.876920,-102.261040(ITA)"));
                activity.startActivity(intent);
            }
        });
        LinearLayout reservacion = (LinearLayout) v.findViewById(R.id.MainRecervacionLinearLayout);
        reservacion.setOnClickListener(new ViewGroup.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                FragmentManager fragmentManager;
                fragment = new FragmentReservacion();
                fragmentManager = activity.getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
            }
        });

        return v;
    }
}
