package com.example.abraham.rgpdroid;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Abraham on 23/11/2016.
 */

public class VisitasListAdapter extends ArrayAdapter {

    public VisitasListAdapter(Context context, List objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_visitas, null);
        }

        ImageView qr = (ImageView) convertView.findViewById(R.id.qrReservacionesImageView);

        TextView NombreDelBar = (TextView) convertView.findViewById(R.id.textView15);
        TextView Fecha = (TextView) convertView.findViewById(R.id.textView16);
        TextView Hora = (TextView) convertView.findViewById(R.id.textView17);

        VisitasItem item = (VisitasItem) getItem(position);
        NombreDelBar.setText(item.NombreDelBar);
        Fecha.setText(item.Fecha);
        Hora.setText(item.Hora);

        return convertView;
    }
}
