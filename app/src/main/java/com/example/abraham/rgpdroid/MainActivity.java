package com.example.abraham.rgpdroid;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<DrawerItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        drawerList = (ListView) findViewById(R.id.left_drawer2);
        tagTitles = getResources().getStringArray(R.array.Tags);
        //Nueva lista de drawer items
        items = new ArrayList<DrawerItem>();
        items.add(new DrawerItem(tagTitles[0],R.mipmap.ic_identity_white_));
        items.add(new DrawerItem(tagTitles[1],R.mipmap.ic_bar));
        items.add(new DrawerItem(tagTitles[2],R.mipmap.ic_event));
        items.add(new DrawerItem(tagTitles[3],R.mipmap.ic_config));
        items.add(new DrawerItem(tagTitles[4],R.mipmap.ic_power));

        // Relacionar el adaptador y la escucha de la lista del drawer
        drawerList.setAdapter(new DrawerListAdapter(this, items));
        drawerList.setOnItemClickListener(new DrawerItemClickListener());

        selectItem(1);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    ListView drawerList;
    String[] tagTitles;

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        View viewAnterior = null;
        int ColorBase;
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
            if (viewAnterior == null)
            {
                ImageView ima = (ImageView)view.findViewById(R.id.icon);
                ima.setColorFilter(Color.BLACK);
                TextView text = (TextView)view.findViewById(R.id.name);
                text.setTextColor(Color.BLACK);

                ColorBase = view.getDrawingCacheBackgroundColor();
                view.setBackgroundColor(Color.rgb(39,153,255));
                viewAnterior = view;
            }
            else
            {
                viewAnterior.setBackgroundColor(ColorBase);
                ImageView ima = (ImageView)viewAnterior.findViewById(R.id.icon);
                ima.setColorFilter(Color.TRANSPARENT);
                TextView text = (TextView)viewAnterior.findViewById(R.id.name);
                text.setTextColor(Color.WHITE);

                view.setBackgroundColor(Color.rgb(39,153,255));
                ima = (ImageView)view.findViewById(R.id.icon);
                ima.setColorFilter(Color.BLACK);
                text = (TextView)view.findViewById(R.id.name);
                text.setTextColor(Color.BLACK);

                viewAnterior = view;
            }
        }
    }

    private void selectItem(int position) {

        Fragment fragment;
        FragmentManager fragmentManager;
        switch (position)
        {
            case 0:
                fragment = new FragmentPerfil();
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
                break;

            case 1:
                fragment = new FragmentAntros();
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
                break;

            case 2:
                fragment = new FragmentReservaciones();
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
                break;

            case 3:
                fragment = new FragmentConfiguracion();
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
                break;

            case 4:
                this.finish();
                break;

            default:
                fragment = new ArticleFragment();
                Bundle args = new Bundle();
                args.putInt(ArticleFragment.ARG_ARTICLES_NUMBER, position);
                fragment.setArguments(args);

                //Reemplazar contenido
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
                break;
        }

        // Se actualiza el item seleccionado y el título, después de cerrar el drawer
        drawerList.setItemChecked(position, true);
        setTitle(tagTitles[position]);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
