package com.example.abraham.rgpdroid;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Abraham on 25/11/2016.
 */

public class FragmentReservacionConfirmada extends Fragment {

    public FragmentReservacionConfirmada()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_reservacion_confirmada, container, false);

        return rootView;
    }
}
