package com.example.abraham.rgpdroid;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abraham on 25/11/2016.
 */

public class ReservacionAdapter extends BaseAdapter
{
    private Activity activity;
    private ArrayList<Reservacion> listItems;

    Fragment fragment;
    FragmentManager fragmentManager;

    public ReservacionAdapter(Activity activity, ArrayList<Reservacion> listItems )
    {
        this.activity = activity;
        this.listItems = listItems;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int i) {
        return listItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.item_reservaciones, null);

        TextView txt1 = (TextView) view.findViewById(R.id.nombreBarReservacionesTextView);
        TextView txt2 = (TextView) view.findViewById(R.id.fechaReservacionesTextView);
        TextView txt3 = (TextView) view.findViewById(R.id.horaReservacionesTextView);

        Reservacion item = (Reservacion) getItem(i);
        txt1.setText(item.getNombreDelBar());
        txt2.setText(item.getFecha());
        txt3.setText(item.getHora());


        ImageView informacionIco = (ImageView) view.findViewById(R.id.qrReservacionesImageView);
        informacionIco.setOnClickListener(new ViewGroup.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "qr", Toast.LENGTH_SHORT).show();
                fragment = new FragmentCodigoQR();
                fragmentManager = activity.getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, fragment).commit();
            }
        });

        return view;
    }
}


class Reservacion
{
    private String nombreDelBar;
    private String nombre;
    private String numeroTelefonico;
    private int numeroDePersonas;
    private String fecha;
    private String hora;
    private String promocion;

    public String getNombreDelBar() {
        return nombreDelBar;
    }

    public void setNombreDelBar(String nombreDelBar) {
        this.nombreDelBar = nombreDelBar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumeroTelefonico() {
        return numeroTelefonico;
    }

    public void setNumeroTelefonico(String numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }

    public int getNumeroDePersonas() {
        return numeroDePersonas;
    }

    public void setNumeroDePersonas(int numeroDePersonas) {
        this.numeroDePersonas = numeroDePersonas;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getPromocion() {
        return promocion;
    }

    public void setPromocion(String promocion) {
        this.promocion = promocion;
    }

    public Reservacion(String nombreDelBar, String nombre, String numeroTelefonico, int numeroDePersonas, String fecha, String hora, String promocion)
    {
        this.nombreDelBar = nombreDelBar;
        this.nombre = nombre;
        this.numeroTelefonico = numeroTelefonico;
        this.numeroDePersonas = numeroDePersonas;
        this.fecha = fecha;
        this.hora = hora;
        this.promocion = promocion;
    }
}
