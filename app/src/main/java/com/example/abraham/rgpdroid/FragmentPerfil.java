package com.example.abraham.rgpdroid;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Abraham on 23/11/2016.
 */

public class FragmentPerfil extends Fragment {
    ArrayList<VisitasItem> category = new ArrayList<VisitasItem>();

    public FragmentPerfil() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_perfil, container, false);
        ListView lv = (ListView) rootView.findViewById(R.id.listView11);

        category.add(new VisitasItem("Bar: Bacanora","Fecha de reservacion: 19-11-2016","Hora de confirmación: 11:00"));

        VisitasListAdapter adapter = new VisitasListAdapter(getActivity(), category);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int pos = position;
                //CODIGO AQUI
            }
        });
        return rootView;
    }

}
